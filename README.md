# Markup and Git: A Writer's Guide

A guide on how to use Git and markup languages to write prose, with a focus on AsciiDoc and Markdown.

## Building The Guide

The guide can be easily rendered in PDF, EPUB, MOBI and HTML using the included `build.sh`. Simply running it via `sh` will create the relevant files in the `out` folder.
