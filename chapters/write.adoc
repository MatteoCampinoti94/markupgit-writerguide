[#write]
== Writing and Git

Now that we have explored the basic usage of Git and repositories and markup syntaxes, we can start exploring how they can help you with writing.

We'll use a short story as an example and we'll walk through all the steps.
