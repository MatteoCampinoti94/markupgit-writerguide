[#examples]
== Examples

This section contains simple examples and examples to help you get familiarized with the Git commands, repository operations and markup syntaxes we have dealt with in this guide.

include::examples/examples_repo.adoc[]
