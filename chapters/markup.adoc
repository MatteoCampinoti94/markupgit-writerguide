[#markup]
== Markup Languages

Now that you have created a repository, it is time to start talking about markup languages.

Markup languages are a wonderful tool that will let you use any text editor, on any device, to write your story and add special formatting, headings, tables, images and even more.

At their core markup languages are plain text files (think txt). They use special keywords to add formatting in an simple, readable way that is also easy to edit.

Markup languages usually translate directly to HTML. This allows for easy building of PDFs, EPUBs, and other formats which use HTML at their core or that can be rendered from it.

include::markup/markup_syntax.adoc[]
