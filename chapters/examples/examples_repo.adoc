[#examples_repo]
=== Repository Examples

==== Repository Initialization and New File Addition

. Create a new folder called _Test Repository_ and open the console inside it.
. Initialize the repository with `*git init*`.
. Add the _README.md_ file with a title and description text.

.README
====
[source,Markdown]
----
# Title

Project description.
----
====

[start=4]
. Add _README.md_ to the staging area with `*git add README.md*`.
. Commit the changes to _README.md_ with message "Add new README" using the command `*git commit -m "Add new README"*`.

==== Commit Changes To A File

. Modify an existing file, let's use _Story.adoc_ for the example.
. Add a line to the file, for example "=== Subchapter A" (level 2 heading in AsciiDoc syntax).
. Add the file to the staging area with `*git add Story.adoc*`.
. You can now keep editing the file and stage more changes, the staged edits won't be removed unless explicitly reset or updated.
. Commit the changes with message "New subchapter" using command `*git commit -m "New subchapter"*`.

==== Reset To A Previous Point

. Open your commit history using `*git log --oneline*` (the `*--oneline*` option makes the output more compact and readable)

.Git log
====
[source]
----
95c6c95 (HEAD -> master) chapters: examples_repo - modify title
2532063 chapters: exercises -> examples - rename exercises to examples
5f17731 chapters: exercises_repo - add title and id
3eb5355 chapters: exercises* - remove exercises_git
----
====

[start=2]
. Look at the commit messages and identify which one you want to reset to.
. Reset the repository and files to two commits behind the current `HEAD` with `*git reset --hard HEAD^^*` or `*git reset --hard 5f17731*`
