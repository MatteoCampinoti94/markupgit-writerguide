[#repo_use]
=== Use a Repository

[#repo_use-structure]
==== Repostory Structure

We can divide a Git repository into three separate sections: current files, staging area, commit history.

===== Current Files

This represents the current status of your files which you can edit, delete or create.

===== Staging Area

This is where your modifications are stored before being committed. Remember that these are not files per se, but the changes made to them.

===== Commit History

This is where all your modifications are stored, you can reset your repository to any of them.

The commit history contains snapshots of your WHOLE repository. Each commit stores the differences relative to the previous state of the repository, not the files themselves.

[#repo_use-operate]
==== Operate on Repository

To use a Git repository as a writing tool we only need 3 commands:

===== Add

Command: `*git add* _file(s)_`

The `add` command adds the current status of a file (or files) to the staging area.

===== Commit

Command: `**git commit -m "**_message_**"**`

The `commit` command commits your staged files with `_message_` as description.

===== Reset

Command: `*git reset* _target_`

The `reset` command resets your repository to a previous stage. The `_target_` is either a specific commit hash (or its first 7 digits) or a relative target (e.g. `*HEAD~3*` will reset your last 3 commits)

TIP: You can also add the `*--hard*` option (i.e. `*git reset --hard* _reset target_`) to the command to also reset the files content. Without the option the files will be left as they currently are and only the commit history will be reset.

WARNING: The commits history is linear, so if you modified _fileA_, then _fileB_, then _fileA_ again, you cannot reset to the first commit without also resetting the commit status of _fileB_ to whatever it was at that point in time. In other words, you can reset the whole repository, not the single files.

WARNING: A reset command CANNOT be undone without very careful handling, do not use it unless you know what you are doing.

===== Log

Command: `*git log*`

The `log` command allows you to see the history of your repository. It shows you the commit hash, message, author and date.

.Git log
====
[source]
----
commit faf8994d707d50fb2c62383739a8a6dacf7a398a (HEAD -> master)
Author: MatteoCampinoti94 <matteo.campinoti94@gmail.com>
Date:   Tue Dec 18 03:53:50 2018 +0100

    chapters: repo_use - add shortenend hash info

commit 22a7ec2f4b7172194cbec3700c4abe5c6476b92f
Author: MatteoCampinoti94 <matteo.campinoti94@gmail.com>
Date:   Tue Dec 18 03:37:57 2018 +0100

    chapters: repo_create - small source attribute edit
----
====

The `HEAD` keyword indicates where the repository is situated now and `master` is the branch of the repository the `HEAD` is situated at.

TIP: A usefull option for the `log` command is `*--oneline*` (i.e. `*git log --oneline*`). This will compact the log into a more readable format, showing only the shrtened hash and message.

.Git log (oneline)
====
[source]
----
95c6c95 (HEAD -> master) chapters: examples_repo - modify title
2532063 chapters: exercises -> examples - rename exercises to examples
5f17731 chapters: exercises_repo - add title and id
3eb5355 chapters: exercises* - remove exercises_git
----
====
