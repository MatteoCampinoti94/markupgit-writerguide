[#git]
== Git

The first thing you will need to do is install Git. To check whether it is already installed on your system you can run `*git --version*` in the console. If Git is installed the output should be `git version _X.Y.Z_`.

If Git is installed you can skip to <<#git_setup,Setup>>.

include::git/git_install.adoc[]

include::git/git_setup.adoc[]
