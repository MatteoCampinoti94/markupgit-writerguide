#!/bin/sh

test_asciidoctor() {
  asciidoctor -v &>/dev/null && echo 1
  asciidoctor-pdf -v &>/dev/null && echo 2
  asciidoctor-epub3 -v &>/dev/null && echo 3
  kindlegen &>/dev/null && echo 4
}

convert_pdf() {
  echo "==> Converting to PDF"
  err_pdf="$(asciidoctor-pdf -o out/Markup\ and\ Git.pdf guide.adoc 2>&1 > /dev/null)" || {
    echo -e "$err_pdf"
  }
}

convert_epub() {
  echo "==> Converting to EPUB"
  err_epub="$(asciidoctor-epub3 -o out/Markup\ and\ Git.epub guide.adoc 2>&1 > /dev/null)" || {
    echo -e "$err_epub"
  }
}

convert_mobi() {
  echo "==> Converting to KF8/MOBI (Amazon Kindle)"
  err_mobi="$(asciidoctor-epub3 -a ebook-format=mobi -o out/Markup\ and\ Git.mobi guide.adoc 2>&1 > /dev/null)" || {
    echo -e "$err_mobi"
  }
}

convert_html() {
  echo "==> Converting to HTML"
  err_html="$(asciidoctor -a source-highlighter=pygments -o out/Markup\ and\ Git.html guide.adoc 2>&1 > /dev/null)" || {
    echo -e "$err_html"
  }
}

dir="$(readlink -f "$0")"
dir="${dir#%/*}"
dir="${dir#$PWD/}"

[[ $dir != $0 ]] && cd -- "$dir"

[[ ! -d out ]] && mkdir out

converters="$(test_asciidoctor)"

[[ $converters =~ .*1.* ]] || echo "not found 'asciidoctor': cannot convert to html"
[[ $converters =~ .*2.* ]] || echo "not found 'asciidoctor-pdf': cannot convert to pdf"
[[ $converters =~ .*3.* ]] || echo "not found 'asciidoctor-epub3': cannot convert to epub"
[[ $converters =~ .*4.* ]] || echo "not found 'kindlegen': cannot convert to mobi"

[[ $converters =~ .*1.* && $@ =~ ^(|.*html.*)$ ]] && convert_html
[[ $converters =~ .*2.* && $@ =~ ^(|.*pdf.*)$ ]]  && convert_pdf
[[ $converters =~ .*3.* && $@ =~ ^(|.*epub.*)$ ]] && convert_epub
[[ $converters =~ .*4.* && $@ =~ ^(|.*mobi.*)$ ]] && convert_mobi

[[ -n $converters ]]
